# GitLab URL
external_url 'http://gitlab.home.arpa'

# Disable Puma Clustered mode
puma['worker_processes'] = 0

# Optimize Sidekiq
sidekiq['max_concurrency'] = 10

# Optimize rails
gitlab_rails['env'] = {
  'MALLOC_CONF' => 'dirty_decay_ms:1000,muzzy_decay_ms:1000'
}

# Optimize gitly
gitaly['cgroups_repositories_count'] = 2
gitaly['cgroups_mountpoint'] = '/sys/fs/cgroup'
gitaly['cgroups_hierarchy_root'] = 'gitaly'
gitaly['cgroups_memory_bytes'] = 500000
gitaly['cgroups_cpu_shares'] = 512

gitaly['concurrency'] = [
  {
    'rpc' => "/gitaly.SmartHTTPService/PostReceivePack",
    'max_per_repo' => 3
  }, {
    'rpc' => "/gitaly.SSHService/SSHUploadPack",
    'max_per_repo' => 3
  }
]
gitaly['env'] = {
  'MALLOC_CONF' => 'dirty_decay_ms:1000,muzzy_decay_ms:1000',
  'GITALY_COMMAND_SPAWN_MAX_PARALLEL' => '2'
}
# Disable monitoring
prometheus_monitoring['enable'] = false