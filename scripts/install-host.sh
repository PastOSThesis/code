#! /usr/bin/bash

dnf install -y epel-release
dnf install -y vim htop

dnf install -y curl policycoreutils openssh-server perl

# Install gitlab-ee with version 15.6.x
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
GITLAB_EE_VERSION="$(sudo dnf list --showduplicates gitlab-ee | awk '{print $2}' | grep "15.6." | tail -n 1)"
EXTERNAL_URL="http://gitlab.home.arpa" dnf install -y "gitlab-ee-${GITLAB_EE_VERSION}"

gitlab-rails runner -e production "puts Gitlab::CurrentSettings.current_application_settings.runners_registration_token" > /vagrant/token-runner.txt

mv "/etc/gitlab/gitlab.rb" "/etc/gitlab/gitlab-default.rb"
cp "/vagrant/configs/gitlab.rb" "/etc/gitlab/gitlab.rb"
sudo gitlab-ctl reconfigure
