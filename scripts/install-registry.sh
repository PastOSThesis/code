#! /usr/bin/bash

dnf install -y epel-release
dnf install -y vim htop

dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
dnf remove -y podman buildah
dnf install -y docker-ce docker-ce-cli containerd.io
systemctl start docker.service
systemctl enable docker.service

docker run -d \
    -p 5000:5000 \
    --name registry \
    registry:2
