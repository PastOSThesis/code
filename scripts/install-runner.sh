#! /usr/bin/bash

PROJECT_REGISTRATION_TOKEN="$(cat /vagrant/token-runner.txt)"

dnf install -y epel-release
dnf install -y vim htop

dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
dnf remove -y podman buildah
dnf install -y docker-ce docker-ce-cli containerd.io
systemctl start docker.service
systemctl enable docker.service

# Install gitlab-runner with version 15.6.x
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
GITLAB_RUNNER_VERSION="$(sudo dnf list --showduplicates gitlab-runner | awk '{print $2}' | grep "15.6." | tail -n 1)"
dnf install -y "gitlab-runner-${GITLAB_RUNNER_VERSION}"

mkdir -p "/etc/docker/"
cp "/vagrant/configs/daemon.json" "/etc/docker/daemon.json"
systemctl restart docker.service

gitlab-runner register \
  --non-interactive \
  --url "http://192.168.0.2/" \
  --registration-token "${PROJECT_REGISTRATION_TOKEN}" \
  --executor "docker" \
  --docker-image "docker:20.10.16" \
  --docker-privileged \
  --docker-volumes "/certs/client" \
  --docker-volumes "/etc/docker/daemon.json:/etc/docker/daemon.json:ro" \
  --description "docker-runner" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
